/* import axios */
import axios from "axios";

/* import Vue */
import { createApp } from "vue";

/* import header components */
import Header from "./components/Header.vue";
import Footer from "./components/Footer.vue";

var headerStaticConfig;
// import headerStaticConfig from config.static.yaml
import headerStaticConfigDefault from "./config.static.yaml";
if (typeof headerStaticConfigInject === "undefined") {
  console.log("rps header: headerStaticConfig undefined, using default config");
  headerStaticConfig = {
    ...headerStaticConfigDefault,
    header_login_url:
      import.meta.env.VITE_RPS_HEADER_SIGN_IN_URL || "/oauth2/start",
    header_logout_url:
      import.meta.env.VITE_RPS_HEADER_SIGN_OUT_URL || "/oauth2/sign_out",
    header_userinfo_url:
      import.meta.env.VITE_RPS_HEADER_USERINFO_URL || "/oauth2/userinfo",
    header_dynamic_config_url:
      import.meta.env.VITE_RPS_HEADER_DYNAMIC_CONFIG_URL || "/config",
  };
} else {
  console.log(
    "rps header: headerStaticConfig is not null, using injected variable"
  );
  headerStaticConfig = headerStaticConfigInject;
}
console.log("rps header: headerStaticConfig", headerStaticConfig);
function createVueApp(appConfig, userinfo, show_toggle) {
  console.log("rps header: creating Vue apps");

  // add header element when id 'rps-header' is not in DOM
  if (document.getElementById("rps-header") == null) {
    var headerElement = document.createElement("nav");
    headerElement.id = "rps-header";
    headerElement.classList.add("rps-header");
    document.body.prepend(headerElement);
  }
  const headerApp = createApp(Header, {
    config: appConfig,
    userinfo: userinfo,
    show_toggle: show_toggle,
  });
  headerApp.mount("#rps-header");
  console.log("rps header: header app created");
}

function getLoginStatus(userinfo_url) {
  console.log(
    "rps header: getting userinfo from oauth2-proxy userinfo endpoint"
  );
  return new Promise((resolve, reject) => {
    axios
      .get(userinfo_url, {
        withCredentials: true,
      })
      .then((res) => {
        if (res.status === 200) {
          var userinfo = res.data;
          console.log("rps header: user is logged in with userinfo:", userinfo);
          resolve(userinfo);
        } else {
          console.log(
            "rps header: got unexpected return code from userinfo endpoint",
            res
          );
          reject(
            "rps header: got unexpected return code from userinfo endpoint",
            res
          );
        }
      })
      .catch((err) => {
        if (err.response.status === 401) {
          console.log(
            "rps header: user is not logged in according to userinfo"
          );
          resolve(false);
        } else {
          console.log(
            "rps header: failed to get userinfo from oauth2-proxy userinfo endpoint",
            err
          );
          reject(err);
        }
      });
  });
}

function filterList(list, userinfo) {
  var isAuthenticated = userinfo != false;
  var userGroup = userinfo.groups ?? [];
  var filteredList = [];
  //   console.log("rps header: filterlist - input", list, userinfo);

  list.forEach((item) => {
    // console.log("rps header: filterlist - item", item);
    // check if login is required for this item
    // and if the user is logged in
    var requireLogin = item.requireLogin ?? false;
    if (requireLogin == true && !isAuthenticated) {
      //   console.log("rps header: filterlist - requireLogin true", filteredList);
      return;
    }

    // check if any required groups are set
    // and if the user is in any of the groups
    // console.log("rps header: filterlist - item.requiredGroups",item.requiredGroups);
    if ("requiredGroups" in item && typeof item.requiredGroups !== undefined) {
      if (item.requiredGroups.length > 0) {
        var userIsInGroup = (item.requiredGroups ?? []).some((value) =>
          userGroup.includes(value)
        );

        // console.log(
        //   "rps header: filterlist - requireLogin userIsInGroup",
        //   userIsInGroup
        // );
        if (!userIsInGroup) {
          return;
        }
      }
    }

    // check for children and filter them
    if (item.children) {
      //   console.log("rps header: filterlist - item.children", item.children);
      item.children = filterList(item.children, userinfo);
      if (item.children.length == 0) {
        delete item.childmenu;
      }
    }
    filteredList.push(item);
  });
  //   console.log("rps header: filterlist - output", filteredList);
  return filteredList;
}

// function ensureStylesheetsInHead(header_stylesheet_url) {
//     // add stylesheet if not already in head
//     var head = document.getElementsByTagName('head')[0];
//     // search for existing stylesheet with id rps-header-stylesheet
//     var existingStylesheet = document.getElementById("rps-header-stylesheet");
//     if (existingStylesheet == null) {
//         var link = document.createElement('link');
//         link.id = "rps-header-stylesheet";
//         link.rel = 'stylesheet';
//         link.type = 'text/css';
//         link.href = header_stylesheet_url;
//         link.media = 'all';
//         head.appendChild(link);
//     }
// }

function calc_toggle_visibility(toggle_items) {
  var href = window.location.href;
  console.log("rps header: calc_toggle_visibility showToggle", showToggle);
  var showToggle = false;
  toggle_items.forEach((item) => {
    showToggle = showToggle || href.includes(item.triggerUrl);
  });
  return showToggle;
}

function loadHeaderClientSide(headerStaticConfig) {
  // Fetch dynamic config
  var headerDynamicConfigPromise = axios.get(
    headerStaticConfig.header_dynamic_config_url,
    {
      withCredentials: true,
    }
  );
  headerDynamicConfigPromise.catch((err) => {
    console.log("rps header: headerDynamicConfigPromise failed", err);
  });

  // wating for document to load
  var documentLoadPromise = new Promise(function (resolve) {
    // if rps-header-dev-script loaded then resolve
    if (document.getElementById("rps-header-dev-script") === undefined) {
      document.addEventListener("DOMContentLoaded", resolve, false);
    }
    resolve(true);
  });
  documentLoadPromise.catch((err) => {
    console.log("rps header: documentLoadPromise failed", err);
  });

  var loginStatusPromise = getLoginStatus(headerStaticConfig.header_userinfo_url)
  loginStatusPromise.catch((err) => {
    console.log("rps header: getLoginStatus failed", err);
  });

  var header_login_url = headerStaticConfig["header_login_url"];
  // if there is no ?rd= in header_login_url, urlencode the current site url to the login url
  if (header_login_url.indexOf("?rd=") === -1) {
    header_login_url += "?rd=" + encodeURIComponent(window.location.href);
    console.log(
      "rps header: appended header login url with redirect",
      header_login_url
    );
  }

  // Wait for all promises to resolve
  Promise.all([headerDynamicConfigPromise, documentLoadPromise, loginStatusPromise])
  .then((values) => {
    console.log("rps header: all promises resolved");
    const headerDynamicConfig = values[0].data;
    const userinfo = values[2];

    var login_hint = document.querySelector("#rps-header").getAttribute("data-user-logged-in");
    if (login_hint !== null) {
      login_hint = !!login_hint;
      console.log("rps header: got login hint", login_hint);
    }
    // do the login hint redirect if the user is not logged in
    if ( login_hint !== null && login_hint && !userinfo ){
      console.log("rps header: redirect to header_login_url", header_login_url)
      window.location.href = header_login_url;
    } else {
      console.log("rps header: rendering the header...")
      //ensureStylesheetsInHead(headerStaticConfig.header_stylesheet_url);
  
      headerDynamicConfig.top_navbar_links = filterList(
        headerDynamicConfig.top_navbar_links,
        userinfo
      );
      headerDynamicConfig.bottom_navbar_links = filterList(
        headerDynamicConfig.bottom_navbar_links,
        userinfo,
        header_login_url
      );
  
      var appConfig = {
        ...headerStaticConfig,
        ...headerDynamicConfig,
        header_login_url,
      };
  
      var showToggle = calc_toggle_visibility(
        appConfig.toggleButtonConfig.config.items
      );
  
      createVueApp(appConfig, userinfo, showToggle);
    }
  })
  .catch((err) => {
    console.log("rps header: promise failed", err);
  });
}

if (typeof headerStaticConfig !== "undefined") {
  loadHeaderClientSide(headerStaticConfig);
}

export { filterList, loadHeaderClientSide, Header, Footer };
