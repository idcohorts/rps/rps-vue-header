#!/usr/bin/env node

console.log("starting header server")

// libraries
import express from "express"
import compression from "compression"
import fs from "fs"
import crypto from "crypto"
import yaml from "yaml"
import styleInject from "style-inject"

import { createServer as createViteServer } from 'vite'

// run a small webserver to serve the header
var app = express();
app.use(compression());

function generateHeaderJS() {
  //header static config
  var headerStaticConfig = {
    ...yaml.parse(fs.readFileSync("./config.static.yaml", "utf8")),
    header_login_url: process.env.RPS_HEADER_SIGN_IN_URL || "/oauth2/sign_in",
    header_logout_url: process.env.RPS_HEADER_SIGN_OUT_URL || "/oauth2/sign_out",
    header_userinfo_url: process.env.RPS_HEADER_USERINFO_URL || "/oauth2/userinfo",
    header_dynamic_config_url: process.env.RPS_HEADER_DYNAMIC_CONFIG_URL || "/config",
  };
  console.log("headerStaticConfig:", headerStaticConfig);

  // injection javascript for the static header config
  const injectConfigJS = `const headerStaticConfigInject=${JSON.stringify(headerStaticConfig)};`;

  // load header.css
  const headerCSS = fs.readFileSync("./dist/style.css", "utf8");
  const injectHeaderCSS = styleInject.toString() + `styleInject(${JSON.stringify(headerCSS)});`;

  // read the header.js file to calculate the checksum
  const headerJS = injectConfigJS + injectHeaderCSS + fs.readFileSync("./dist/rps-vue-header.umd.cjs");

  // tell us how large the header is in Mebibytes
  console.log(
    "header size: ",
    (headerJS.length / 1024 / 1024).toFixed(2) + " MiB"
  );

  return headerJS;
}

// when the server runs in dev mode, do not redirect to the checksum file but serve the header directly
if ((process.env.RPS_HEADER_WEBSERVER_ENV || "dev") === "dev") {
  console.log("running in dev mode, serving header directly")


  // create a vite server
  const vite = await createViteServer({
    server: { middlewareMode: true },
    appType: 'custom'
  });
  app.use(vite.middlewares);
  
  // if running in dev mode execute header-dev.js to insert main.js
  app.get("/header.js", function (req, res) {
    var headerdev = fs.readFileSync("./header-dev.js");
    // do not cache
    res.set("Cache-Control", "no-cache");
    // send content of header-dev.js
    res.send(headerdev);
  });
  // provide main.js
  app.get("/main.js", function(req,res){
    var main = fs.readFileSync("./main.js");
    // do not cache
    res.set("Cache-Control", "no-cache");
    // send content of main.js
    res.send(main);
  })

    
  // provide a fake userinfo endpoint for testing
  var loginState = false;
  app.get("/test/userinfo", function (req, res) {
    if (loginState) {
      // read userinfo.test.json
      var userinfo = JSON.parse(fs.readFileSync("./userinfo.test.json"));
      res.json(userinfo);
    } else {
      res.status(401).send("Unauthorized");
    }
  });

  // provide a fake userinfo endpoint for sign in
  app.get("/test/sign_in", function (req, res) {
    console.log("signing in on test endpoint");
    loginState = true;
    res.redirect("/test");
  });

  // provide a fake userinfo endpoint for sign out
  app.get("/test/sign_out", function (req, res) {
    console.log("signing out on test endpoint");
    loginState = false;
    res.redirect("/test");
  });

  // redirect /oauth2/userinfo to /test/userinfo
  app.get("/oauth2/userinfo", function (req, res) {
    res.redirect("/test/userinfo");
  });

  // redirect /oauth2/sign_in to /test/sign_in
  app.get("/oauth2/sign_in", function (req, res) {
    res.redirect("/test/sign_in");
  });

  // redirect /oauth2/sign_out to /test/sign_out
  app.get("/oauth2/sign_out", function (req, res) {
    res.redirect("/test/sign_out");
  });

// when the server runs in prod mode, calculate the checksum and redirect to the checksum file
} else {
  console.log(
    "running in prod mode, serving header via checksum with cache control"
  );

  const headerJS = generateHeaderJS();

  // calculate the checksum of the header file
  var hash = crypto.createHash("sha256");
  hash.update(headerJS);
  var checksum = hash.digest("hex");
  // print the checksum
  console.log("header checksum: " + checksum);
  
  app.get("/header.js", function (req, res) {
    // do not cache
    res.set("Cache-Control", "no-cache");
    // do a redirect from /header.js to /header-$checksum.js
    res.redirect("/header-" + checksum + ".js");
  });

  // serve the header on /header-$checksum.js
  app.get("/header-" + checksum + ".js", function (req, res) {
    // set a javascript content type
    res.set("Content-Type", "application/javascript");
    // set a cache control header so the browser caches the file for a long time
    res.set("Cache-Control", "public, max-age=31536000");
    // send the header from the headerJS variable
    res.send(headerJS);
  });
}

// redirect /dist/header.js to /header.js
app.get("/dist/header.js", function (req, res) {
  res.redirect("/header.js");
});

// redirect /js/header.js to /header.js
app.get("/js/header.js", function (req, res) {
  res.redirect("/header.js");
});

// serve the test.html file on / and /test
app.get(["/", "/test"], function (req, res) {
  console.log("test.js - serving test.html");
  // if running in dev mode, serve the test.html file
  if ((process.env.RPS_HEADER_WEBSERVER_ENV || "dev") === "dev") {
    // do not use __dirname here, because it is not supported by esm
    res.sendFile("./test.html", { root: "." });
  } else {
    // if running in prod mode, servce the test.html file
    // do not use __dirname here, because it is not supported by esm
    res.sendFile("./test.html", { root: "." });
  }
});

// serve the howto.html file on /howto
app.get("/howto", function (req, res) {
  // read howto.html
  var howto = fs.readFileSync("./howto.html", "utf8");
  // replace the placeholder with the header url
  howto = howto.replace(
    "$RPS_HEADER_JS_URL",
    process.env.RPS_HEADER_JS_URL || "/header.js"
  );
  // also replace the second placeholder with the header url
  howto = howto.replace(
    "$RPS_HEADER_JS_URL",
    process.env.RPS_HEADER_JS_URL || "/header.js"
  );
  // replace RPS_HEADER_ADMIN_CONTACT_LINK
  howto = howto.replace(
    "$RPS_HEADER_ADMIN_CONTACT_LINK",
    process.env.RPS_HEADER_ADMIN_CONTACT_LINK || "mailto:admin@example.org"
  );
  // send the howto file
  res.set("Content-Type", "text/html");
  res.send(howto);
});

// serve the test-login-hint.html file on /test-login-hint
app.get("/test-login-hint", function (req, res) {
  var howto = fs.readFileSync("./test-login-hint.html", "utf8");
  res.set("Content-Type", "text/html");
  res.send(howto);
});

// serve the header config on /config
app.get("/config", function (req, res) {
  // no cache control header, so the browser does not cache the file
  res.set("Cache-Control", "no-cache");
  // load config.yaml
  const headerDynamicConfig = yaml.parse(
    fs.readFileSync("./config.dynamic.yaml", "utf8")
  );
  console.log("serving header config", headerDynamicConfig);
  // send a json response
  res.json(headerDynamicConfig);
});

// serve the header config file on /config.yaml
app.get("/config.yaml", function (req, res) {
  // set a yaml content type
  res.set("Content-Type", "text/yaml");
  // do not use __dirname here, because it is not supported by esm
  res.sendFile("./config.yaml", { root: "." });
});

// serve the favicon on /favicon.ico
app.get("/favicon.ico", function (req, res) {
  // do not use __dirname here, because it is not supported by esm
  res.sendFile("./assets/favicon.ico", { root: "." });
});

// start the server
app.listen(4080, function () {
  console.log("header server listening on port 4080!");
});
