import vue from '@vitejs/plugin-vue'
import ViteYaml from '@modyfi/vite-plugin-yaml'

export default {
  plugins: [vue(), ViteYaml()],
  build: {
    lib: {
      entry: 'main.js',
      name: 'Header',
    },
  },
  // fix weird vue bugs in production mode, see https://vitejs.dev/guide/build.html
  define: { 'process.env.NODE_ENV': '"production"' }
}
