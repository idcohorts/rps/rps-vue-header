# rps-vue-header

The VueJS header which is in use for the new DZIF TI BB and NUM plattform. 

If you want to customize the header for a project please consider the following files:
- config.yaml
- config.scss
- 3 logos files in asset folder

## Docker based local dev env
One command and you are ready to develop:
```
docker-compose --profile dev up
```
go to http://localhost:4080/test

It also does live reload when you change the source code and provides you with a fake userinfo endpoint for testing. You can edit the response in the userinfo.test.json file.

## Download current header configuration from a live project for testing
The webserver provides a /config.yaml endpoint where you can download the current configuration, for example on https://header.dev.numhub.de/config.yaml you get the current header config of the NUMhub dev env.

## Test the Docker production image
```
docker-compose --profile prod up --build
```
go to http://localhost:4080/test

## Local project setup (not recomended nor supported)
```
yarn install
```

### Compiles and hot-reloads for development per Rollup

```
yarn run watch
```

### Compile header

```
yarn run build
```

### Run the webserver
```
yarn run webserver
```
Go to http://localhost:4080/test

### Watch out

Please change the script values to the following in the ***package.json***.
```
"scripts": {
    "build": "node_modules/rollup/dist/bin/rollup -c",
    "watch": "node_modules/rollup/dist/bin/rollup -c -w"
  },
```

# Todos:
- [ ] crate easy project theme integration  
- [ ] create meaningful example config for idcohorts.net
- [ ] allow to integrate custom html blocks for footer

Possible headers to migrate:
- [ ] NAPKON header
- [ ] ORCHESTRA header
- [ ] dzg.digital header