# build header with an nodejs image
FROM node:18

RUN mkdir -p /opt/header
WORKDIR /opt/header

COPY package.json yarn.lock ./
RUN yarn install

ENV RPS_HEADER_WEBSERVER_ENV=prod

COPY . .
RUN yarn build
CMD yarn --offline webserver
